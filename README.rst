==========================
Python Package sbrfeatures
==========================

This package was developed within a research project aiming at monitoring small,
unstaffed wastewater treatment plants of the type sequencing batch reactor (SBR)
with unmaintained sensors (see [Schneider2019]_).

The sbrfeatures package is designed to automatically analyse an online
measurement signal by first smoothing it, creating a spline representation and
than search for specific features such as a minimum, maximum or a ramp.

.. [Schneider2019] Schneider, M. Y., Carbajal, J. P., Furrer, V., Sterkele, B., Maurer, M., & Villez, K. (2018). Beyond signal quality: The value of unmaintained pH, dissolved oxygen, and oxidation-reduction potential sensors for remote performance monitoring of on-site sequencing batch reactors. DOI . Preprint doi.org/10.31224/osf.io/ndm7f

Quickstart
==========

Install the latest version of sbrfeatures::

    pip install sbrfeatures

Further Information
===================

Check beyondsignalquality_ subproject for examples with the data_ from a two
population equivalent sequencing batch reactor.

.. _beyondsignalquality: https://gitlab.com/sbrml/beyondsignalquality

.. _data: https://data.eawag.ch/dataset/data-for-the-value-of-unmaintained-sensors

For more information check the documentation_ .

.. _documentation: https://sbrml.gitlab.io/sbrfeatures
