# vim: set tabstop=4
# features_pH.py
#!/usr/bin/env python3

""" Implementation of features for the pH signal."""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>


############
## Imports
# 3rd party
import numpy as np

# user
from .basicfunctions import (
    smooth,
    signal_extrema,
    argrelmax
)

############

## Features
def aeration_valley(time, signal, *, t_interval=None, smoother=None,
        minfinder=None, globalmin=False):
    """Find minimum using a smoothed version of the input signal.

    The aeration valley (or ammonium valley (Al-Ghusain et al. 1994))
    is a minimum (valley) of the pH signal during the
    aeration phase. The pH signal can be noisy and present oscillations
    due to the intermittent aeration method. Hence the minimum is searched
    in a smoothed version of the signal.

    The signal is assumed to be sampled with constant sampling rate.

    This feature is meant to be extracted from pH signals during the aeration
    phase.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        pH signal during aeration phase.

    Keyword arguments
    -----------------
    t_interval : array_like, optional
        Initial and final time indicating the valid time interval. A valley
        found outside of this interval is ignored. Default is the whole input
        time
    smoother : function, optional
        A function used to smooth the signal with the signature
        `smooth_signal = smoother(time, signal)`
        The default smoother is the function :func:`basicfunctions.smooth`
    minfinder : function, optional
        A function used to find minima with the signature
        `t, val = finder(t, signal)`
        The default finder is :func:`basicfunctions.signal_extrema`
    gobalmin : bool, optional
        Use minimum value of the signal if no local minimum was found; the 
        feature returns None only if the detection happens outside of the
        `t_interval` parameter.
        The default is False, i.e. the feature is a local minimum of the signal;
        if the signal does not have a local minimum, the feature will return 
        None.

    Returns
    -------
    float
        indicating the time of the minimum.
    float
        value of the pH signal at its minimum,
        i.e. `pHmin = signal(tmin)`.
    array
        smoothed signal used for the search.

    See also
    --------
    smooth: smoothing of the signal with :func:`basicfunctions.smooth`.
    signal_extrema: Find maxima or minima in a smooth signal
        :func:`basicfunctions.signal_extrema`.
    Literature: Al-Ghusain, I.A., Huang, J., Hao, O.J., Lim, B.S., 1994.
        Using pH as a real-time control parameter for wastewater treatment
        and sludge digestion processes. Water Sci. Technol. 30, 159–168.
        https://doi.org/10.2166/wst.1994.0182.
    """
    if smoother is None:
        smoother = smooth
    signal = smoother(time, signal)

    # find minima using derivatives (local minima)
    if minfinder is None:
        # use signal_extrema as default
        # the follwoing function wraps signal_extrema to confirm to the
        # minfinder signature
        def minfinder_fun(t, s):
            _, tm, sm, _ = signal_extrema(t, s, extrema_type='min')
            return tm, sm
        minfinder = minfinder_fun
    tmin_, pHmin_ = minfinder(time, signal)

    tmin = None
    pHmin = None
    # Take the minimum of the local minima
    if tmin_ is not None:
        i = np.argmin(pHmin_)
        tmin = tmin_[i]
        pHmin = pHmin_[i]

    # if none found then use lowest value of the function
    if (tmin is None) and (globalmin is True):
        idx = argrelmax(np.max(signal) - signal)[0]
        if len(idx) != 0:
            i = np.argmin(signal[idx])
            tmin = time[idx[i]]
            pHmin = signal[idx[i]]

    # report only values within time_interval
    if t_interval is not None and tmin is not None:
        if (tmin < t_interval[0]) or (tmin > t_interval[1]):
            tmin = None
            pHmin = None

    return tmin, pHmin, signal

if __name__ == '__main__':
    # This main is exceuted using 'python -m sbrfetures.features_pH'
    import matplotlib.pyplot as plt

    # first example
    t = np.linspace (0, 1, 100)
    x = np.linspace(0, 1, 8)
    s = np.interp(t, x, np.array([0, 0, 1, -0.5, -0.5, -1, 0, 0]) + x)
    tv, vv, ss = aeration_valley(t, s)

    plt.figure()
    plt.plot(t, s, label='signal')
    plt.plot(t, ss, label='smoothed')
    plt.plot(tv, vv, 'go')
    plt.xlabel('time')
    plt.ylabel('value')
    plt.legend()
    plt.autoscale(enable=True, axis='x', tight=True)
    plt.show()

    # second example
    s = 1 + np.sin(2*np.pi*t*3)*np.exp(-9*t) + \
        (1 + 0.05*np.sin(2*np.pi*10*t)) / (1 + np.exp(-3*(t-0.5)))
    w = int(np.round(0.1 / (t[1]-t[0])))
    tv, vv, ss = aeration_valley(t, s)

    plt.figure()
    plt.plot(t, s, label='signal')
    plt.plot(t, ss, label='smoothed')
    plt.plot(tv, vv, 'go')
    plt.xlabel('time')
    plt.ylabel('value')
    plt.legend()
    plt.autoscale(enable=True, axis='x', tight=True)
    plt.show()

    s = np.linspace(1, 0, 100)
    tv, vv, ss = aeration_valley(t, s)
    plt.figure()
    plt.plot(t, s, label='signal')
    plt.plot(t, ss, label='smoothed')
    plt.plot(tv, vv, 'go')
    plt.xlabel('time')
    plt.ylabel('value')
    plt.legend()
    plt.autoscale(enable=True, axis='x', tight=True)
    plt.show()
